package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentStoresBinding

class StoresFragment : Fragment() {

    private var _binding: FragmentStoresBinding? = null
    private val binding get() = _binding!!

    val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        _binding = FragmentStoresBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Checkout Options")

        val adapter: StoreAdapter

        if (model.adapter == null)
        {
            adapter = StoreAdapter(this, model)
            model.adapter = adapter
        }
        else {
            model.adapter!!.fragment = this
            adapter = model.adapter!!
        }

        adapter.stores.forEach { store ->
            store.updatePrice()
        }

        val recView = binding.recyclerView
        recView.layoutManager = LinearLayoutManager(context)
        recView.adapter = adapter



        // Inflate the layout for this fragment
        return binding.root
    }
}
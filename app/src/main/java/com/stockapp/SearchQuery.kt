package com.stockapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "queries")
data class SearchQuery(@ColumnInfo(name = "text") @PrimaryKey val text: String)

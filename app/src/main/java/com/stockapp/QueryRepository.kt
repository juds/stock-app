package com.stockapp

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class QueryRepository(private val queryDao: QueryDao, private val context: Context) {

    val queries: LiveData<MutableList<SearchQuery>> = queryDao.getAll()

    @WorkerThread
    fun insert(query: SearchQuery) = queryDao.insertQuery(query)

    @WorkerThread
    fun delete(query: SearchQuery) = queryDao.delete(query.text)

    @WorkerThread
    fun deleteAll() = queryDao.deleteAll()
}
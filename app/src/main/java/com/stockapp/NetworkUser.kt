package com.stockapp

data class NetworkUser(
    val admin: Boolean,
    val email: String,
    val name: String,
    val password: String,
    val public_id: String,
    val zipcode: String
)
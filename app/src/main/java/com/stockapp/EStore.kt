package com.stockapp

enum class EStore {
    Kroger {
        override val index = 0
    },
    Target {
        override val index = 1
    },
    Walmart {
        override val index = 2
    },
    Amazon {
        override val index = 3
    };

    abstract val index: Int
}
package com.stockapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.stockapp.databinding.FragmentLoginBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        //I added this line of code; it changes the text at the top of the screen to say "Login"
        // You can change or remove it if you want
        (activity as MainActivity).changeTitle("Login")

        binding.buttonLogin.setOnClickListener {
            val user = User(binding.textUsername.text.toString(), binding.textPassword.text.toString())
            RetrofitService.create(MainActivity.url).getLoginToken(user.encode())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ token ->
                    Toast.makeText(context, "Successfully logged in.", Toast.LENGTH_SHORT).show()
                    val sharedPref = requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
                    with (sharedPref.edit()) {
                        putString("token", token.token)
                        apply()
                    }
                },
                {
                    Log.e("Login", "Error fetching token: $it. Auth code was: ${user.encode()}")
                    Toast.makeText(context, "There was an error logging in.", Toast.LENGTH_SHORT).show()
                })
        }
        binding.buttonRegister.setOnClickListener {
            val user = User(binding.textUsername.text.toString(), binding.textPassword.text.toString())
            //model.register(user, activity as MainActivity)
        }

        // Inflate the layout for this fragment
        return binding.root
    }
}
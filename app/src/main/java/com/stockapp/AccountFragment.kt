package com.stockapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.stockapp.databinding.FragmentAccountBinding
import kotlinx.android.synthetic.main.activity_main.*

class AccountFragment : Fragment() {

    private var _binding: FragmentAccountBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentAccountBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Account")
        (activity as MainActivity).bottomNavigationView.menu.getItem(0).isChecked = true

        val sharedPref = requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "") ?: "NONE"

        var user: NetworkUser

        model.getUser(token, activity as MainActivity, findNavController(), sharedPref)

        model.user.observe(viewLifecycleOwner) {
            user = it
            binding.welcomeText.text = "Welcome back ${user.name}!"
            binding.emailText.text = "Email:\n${user.email}"
            binding.zipcodeText.text = "Zip Code:\n${user.zipcode}"
        }

        binding.clearHistoryButton.setOnClickListener {
            val newDialog = ClearQueriesDialogFragment()
            newDialog.show((activity as MainActivity).supportFragmentManager, "clear queries")
        }

        binding.delTokenButton.setOnClickListener {
            val newDialog = LogoutDialogFragment(findNavController(), sharedPref)
            newDialog.show((activity as MainActivity).supportFragmentManager, "logout")
        }

        // Inflate the layout for this fragment
        return binding.root
    }
}
package com.stockapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CartAdapter(val model: ProductViewModel, val fragment: Fragment): RecyclerView.Adapter<CartAdapter.CartViewHolder>() {

    private var cart = mutableListOf<Item>()

    inner class CartViewHolder(val view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.cart_card, parent, false)
        return CartViewHolder(v)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.product).text = cart[position].description

        Glide.with(fragment).load(cart[position].image).apply(RequestOptions().override(128, 128)).into(holder.view.findViewById(R.id.image))

        holder.view.findViewById<ImageButton>(R.id.removeCartButton).setOnClickListener {
            model.cartDelete(cart[position])
            cart.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, itemCount)
        }

        holder.view.findViewById<ConstraintLayout>(R.id.layout).setOnClickListener {
            navigate(cart[position].description)
        }
    }

    private fun navigate(description: String) {
        model.selectedGroup = description
        Log.d("CartAdapter", "Navigating to group preview with description $description")
        fragment.findNavController().navigate(R.id.groupPreviewFragment)
    }

    override fun getItemCount(): Int {
        return cart.size
    }

    fun setCart(items: List<Item>) {
        cart = items.toMutableList()
    }
}
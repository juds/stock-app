package com.stockapp

data class CartItem(
    val description: String,
    val imageLarge: String,
    val imageSmall: String,
    val stock: Int,
    val price: Float,
    val productGroupId: Int,
    val store: String
)
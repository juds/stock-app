package com.stockapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "item_table")
data class Item(
    @ColumnInfo(name = "store") val store: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "price") val price: Float,
    @PrimaryKey @ColumnInfo(name = "id") val id: Int
    //@ColumnInfo(name = "productId") val productId: String
)
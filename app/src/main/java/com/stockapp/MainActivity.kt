package com.stockapp

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.text.HtmlCompat
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.color.MaterialColors

class MainActivity : AppCompatActivity() {

    companion object {
        const val url: String = "http://18.219.225.200/"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        setContentView(R.layout.activity_main)
        val typedVal = TypedValue()
        theme.resolveAttribute(R.attr.colorPrimary, typedVal, true)
        window.statusBarColor = typedVal.data
        val host: NavHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment?
            ?: return
        setUpBottomNavMenu(host.navController)

        val sharedPref = applicationContext.getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", null)
        if (token == null)
        {
            host.navController.navigate(R.id.popup_login)
            changeTitle("Login")
        }
        else
        {
            changeTitle("Search")
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    private fun setUpBottomNavMenu(navController: NavController)
    {
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val options = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.nav_default_enter_anim)
            .setExitAnim(R.anim.nav_default_exit_anim)
            .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
            .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
            .build()

        bottomNav.setOnItemSelectedListener { item ->
            when(item.itemId) {
                R.id.cartFragment -> navController.navigate(R.id.cartFragment, null, options)
                R.id.searchFragment -> navController.navigate(R.id.searchFragment, null, options)
                R.id.loginFragment -> navController.navigate(R.id.accountFragment, null, options)
                R.id.favoritesFragment -> navController.navigate(R.id.favoritesFragment, null, options)
            }
            true
        }
        bottomNav.setOnItemReselectedListener { /* do nothing; we can change this if we want but basically there's no reason to do something when you reselect a button at the moment */ }

        navController.addOnDestinationChangedListener {_, destination, _ ->
            when (destination.id)
            {
                R.id.priceComparisonMockupFragment -> bottomNav.visibility = View.GONE
                R.id.storesFragment -> bottomNav.visibility = View.GONE
                R.id.loginFragment -> bottomNav.visibility = View.GONE
                R.id.registerFragment -> bottomNav.visibility = View.GONE
                R.id.loginPopupFragment -> bottomNav.visibility = View.GONE
                else -> bottomNav.visibility = View.VISIBLE
            }
        }

        bottomNav.selectedItemId = R.id.searchFragment
    }

    //When called from another area, changes the text that appears on the top bar. I had this from an old project, figured I may as well add it since we'll use it.
    fun changeTitle(newTitle: String)
    {
        val colorInt = MaterialColors.getColor(applicationContext, R.attr.colorOnPrimary, Color.WHITE)
        val colorStr = "#" + "%x".format(colorInt).substring(2)
        supportActionBar!!.title =
            HtmlCompat.fromHtml("<font color=\"$colorStr\">$newTitle</font>", HtmlCompat.FROM_HTML_MODE_COMPACT)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        Log.d("MainActivity", "Changing actionbar title to $newTitle")
    }
}
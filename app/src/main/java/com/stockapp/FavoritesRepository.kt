package com.stockapp

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class FavoritesRepository(private val favDao: ItemDao, private val context: Context) {

    val favorites: LiveData<List<Item>> = favDao.getAll()

    @WorkerThread
    fun insert(item: Item) {
        favDao.insertItem(item)
        val sharedPref = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "")
        RetrofitService.create(MainActivity.url).addToFavs(item, token!!).execute()
    }

    @WorkerThread
    fun deleteItem(item: Item) {
        favDao.delete(item.description)
        val sharedPref = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "")
        RetrofitService.create(MainActivity.url).removeFromFavs(item.description, token!!).execute()
    }

    fun contains(item: Item): LiveData<Boolean> = favDao.contains(item.description)

}
package com.stockapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView

class QueryAdapter(val searchView: SearchView, val model: ProductViewModel, val fragment: Fragment):
    RecyclerView.Adapter<QueryAdapter.QueryViewHolder>(),
    Filterable {

    var queries: MutableList<SearchQuery> = mutableListOf()
    var allQueries: MutableList<SearchQuery> = mutableListOf()

    inner class QueryViewHolder(val view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QueryViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.query_card, parent, false)
        return QueryViewHolder(v)
    }

    override fun onBindViewHolder(holder: QueryViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.searchText).text = queries[position].text
        holder.view.findViewById<ImageButton>(R.id.clear).setOnClickListener {
            val toDelete = queries[position]
            model.queryDelete(toDelete)
            queries.removeAt(position)
            allQueries.remove(toDelete)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, itemCount)
        }

        holder.view.findViewById<TextView>(R.id.searchText).setOnClickListener {
            onClick(queries[position].text)
        }

        holder.view.findViewById<ConstraintLayout>(R.id.layout).setOnClickListener {
            onClick(queries[position].text)
        }
    }

    override fun getFilter(): Filter
    {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val str = constraint.toString()
                queries = if (str.isEmpty()) allQueries else {
                    val resultList = ArrayList<SearchQuery>()
                    for (query in allQueries) {
                        if (query.text.lowercase().contains(str.lowercase())) resultList.add(query)
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = queries
                return filterResults
            }

            override fun publishResults(str: CharSequence?, results: FilterResults?) {
                queries = results?.values as MutableList<SearchQuery>
                notifyDataSetChanged()
            }
        }
    }

    fun setList(newList: MutableList<SearchQuery>) {
        queries = newList
        allQueries = newList
    }

    private fun onClick(str: String) {
        searchView.setQuery(str, true)
    }

    override fun getItemCount(): Int {
        return queries.size
    }
}
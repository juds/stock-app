package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentSearchBinding
import kotlinx.android.synthetic.main.activity_main.*

class SearchFragment : Fragment() {

    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Search")
        (activity as MainActivity).bottomNavigationView.menu.getItem(2).isChecked = true

        val adapter = SearchAdapter(model, this)
        val recView = binding.recView
        recView.layoutManager = LinearLayoutManager(context)
        recView.adapter = adapter

        val queryAdapter = QueryAdapter(binding.searchView, model, this)
        val queryRecView = binding.queryRecView
        queryRecView.visibility = View.VISIBLE
        queryRecView.adapter = queryAdapter
        queryRecView.layoutManager = LinearLayoutManager(context)

        model.queries.observe(viewLifecycleOwner) {queries ->
            queries.reverse()
            queryAdapter.setList(queries)
            queryAdapter.notifyItemRangeChanged(0, queryAdapter.itemCount)
        }

//        ArrayAdapter.createFromResource(requireContext(), R.array.sort_by, android.R.layout.simple_spinner_item).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            binding.spinner.adapter = adapter
//        }
//
//        ArrayAdapter.createFromResource(requireContext(), R.array.filter, android.R.layout.simple_spinner_item).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            binding.spinner2.adapter = adapter
//        }

        /**
         * Planning:
         *
         * Have a boolean that is initially false called something like "hasSearchedYet".
         * When hasSearchedYet is false, a different recyclerview than the one that shows the
         * results from the database will be shown, which has a list of past searches.
         * When onQueryTextChange (below) fires, refine that list to only the ones that match
         * the submitted text.
         * Create a room database that holds all previous searches, and provides it to the new
         * recyclerview. You can do searching/filtering exactly as it was done in that one
         * assignment from mobile dev (I think it was the movie one but don't know for sure).
         * Once onQueryTextSubmit runs, set hasSearchedYet to true, and make the new recyclerview's
         * visibility View.GONE. This way, it won't show up around the search results.
         * It will only appear again once the fragment is recreated (when the user leaves and comes back).
         *
         * This may be helpful https://stackoverflow.com/questions/13733460/android-providing-recent-search-suggestions-without-searchable-activity
         *
         */

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                //query database
                Toast.makeText(context, "Searching...", Toast.LENGTH_LONG).show()
                if (query != null && query != "") {
                    adapter.setItems(listOf())
                    binding.progressBar.visibility = View.VISIBLE
                    adapter.executeSearch(query, binding.progressBar)
                    model.queryInsert(SearchQuery(query))
                    binding.queryRecView.visibility = View.GONE
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                queryAdapter.filter.filter(newText)
                binding.queryRecView.visibility = View.VISIBLE
                return true
            }
        })

        (activity as MainActivity).onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                binding.queryRecView.visibility = View.GONE
            }
        })

        return binding.root
    }

}
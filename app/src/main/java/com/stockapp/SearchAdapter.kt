package com.stockapp

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonSyntaxException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchAdapter(val model: ProductViewModel, var fragment: Fragment): RecyclerView.Adapter<SearchAdapter.ItemViewHolder>() {

    private var items: List<Item> = listOf()
    fun getItems() = items
//    = listOf(
//        Item("Kroger", "Kroger\u00ae 1% Lowfat Milk", "https://www.kroger.com/product/images/thumbnail/left/0001111041660", 3.29f, "0001111041660"),
//        Item("Simple Truth Organic", "Simple Truth Organic\u00ae Whole Milk", "https://www.kroger.com/product/images/small/front/0001111042908", 5.99f, "0001111042908"),
//        Item("Fairlife", "Fairlife 2% Reduced Fat Ultra Filtered Lactose Free Milk", "https://www.kroger.com/product/images/large/front/0085631200277", 4.99f, "0085631200277")
//    )

    inner class ItemViewHolder(val view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.search_card, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val product = holder.view.findViewById<TextView>(R.id.product)
        product.text = items[position].description
        product.isSelected = true
        holder.view.findViewById<TextView>(R.id.price).text = model.formatPrice(items[position].price)

        Glide.with(fragment).load(items[position].image).apply(RequestOptions().override(128, 128)).into(holder.view.findViewById(R.id.image))

        holder.view.findViewById<ImageButton>(R.id.addCartButton).setOnClickListener {
//            items.add(items[position])
            model.cartInsert(items[position])

        }

        model.isFavorite(items[position]).observe(fragment.viewLifecycleOwner) { isFav ->
            val button = holder.view.findViewById<ImageButton>(R.id.favButton)
            if (isFav)
            {
                button.setImageResource(R.drawable.ic_fav)
                button.setOnClickListener {
                    unfavorite(items[position], button)
                }
            }
            else
            {
                button.setImageResource(R.drawable.ic_fav_border)
                button.setOnClickListener {
                    favorite(items[position], button)
                }
            }
        }

        holder.view.findViewById<ImageButton>(R.id.favButton).setOnClickListener {
            favorite(items[position], it as ImageButton)
        }

        holder.view.findViewById<ConstraintLayout>(R.id.layout).setOnClickListener {
            navigate(items[position].description)
        }
    }

    private fun navigate(description: String) {
        model.selectedGroup = description
        Log.d("SearchAdapter", "Navigating to group preview with description $description")
        fragment.findNavController().navigate(R.id.groupPreviewFragment)
    }

    private fun favorite(item: Item, button: ImageButton)
    {
        button.setImageResource(R.drawable.ic_fav)
        model.favInsert(item)
        Toast.makeText(fragment.context, "Added to favorites.", Toast.LENGTH_SHORT).show()
        model.isFavorite(item)
    }

    private fun unfavorite(item: Item, button: ImageButton)
    {
        button.setImageResource(R.drawable.ic_fav_border)
        model.favDelete(item)
        Toast.makeText(fragment.context, "Removed from favorites.", Toast.LENGTH_SHORT).show()
        model.isFavorite(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun executeSearch(search: String, pb: ProgressBar)
    {
        //setItems(listOf())
        Log.d("SearchAdapter", "Executing search with query $search")
        val sharedPref = fragment.requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "") ?: ""
        RetrofitService.create(MainActivity.url).searchProducts(search, token)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                result -> setItems(result)
                pb.visibility = View.GONE
            },
            {
                error ->
                if (error is JsonSyntaxException)
                {
                    Log.e("SearchAdapter", "Credentials invalid: $error")
                    Toast.makeText(fragment.context, "Error establishing account credentials.", Toast.LENGTH_LONG).show()
                    with(sharedPref.edit())
                    {
                        remove("token")
                        apply()
                    }
                    fragment.findNavController().popBackStack(R.id.searchFragment, true)
                    fragment.findNavController().navigate(R.id.loginPopupFragment)
                }
                else
                {
                    showError(error)
                    pb.visibility = View.GONE
                }
            })

    }

    fun setItems(result: List<Item>?)
    {
        Log.d("SearchAdapter", "Updating items list; new list is $result")
        notifyItemRangeChanged(0, itemCount)
        items = result!!
        notifyItemRangeChanged(0, itemCount)
    }

    fun showError(error: Throwable?) {
        Log.e("SearchAdapter", "Error fetching from API: $error")
        Toast.makeText(fragment.context, "Could not reach the server.", Toast.LENGTH_LONG).show()
    }
}
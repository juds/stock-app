package com.stockapp

import android.content.Context
import android.util.Log
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData

class CartItemRepository(private val cartDao: ItemDao, private val context: Context) {

    val cart: LiveData<List<Item>> = cartDao.getAll()

    @WorkerThread
    fun insert(item: Item) {
        cartDao.insertItem(item)
        val sharedPref = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "")
        val response = RetrofitService.create(MainActivity.url).addToCart(item, token!!).execute()
        Log.d("repo", "Inserting cart item $item")
    }

    @WorkerThread
    fun deleteItem(item: Item)
    {
        cartDao.delete(item.description)
        val sharedPref = context.getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "")
        val response = RetrofitService.create(MainActivity.url).removeFromCart(item.description, token!!).execute()
        Log.d("repo", "deleting cart item $item")
    }
}
package com.stockapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class AltAdapter(val model: ProductViewModel, val fragment: Fragment): RecyclerView.Adapter<AltAdapter.ItemViewHolder>() {

    var items: List<CartItem> = listOf()

    inner class ItemViewHolder(val view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AltAdapter.ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.alt_card, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: AltAdapter.ItemViewHolder, position: Int) {
        val product = holder.view.findViewById<TextView>(R.id.product)
        product.text = items[position].description
        product.isSelected = true
        holder.view.findViewById<TextView>(R.id.price).text = model.formatPrice(items[position].price)

        Glide.with(fragment).load(items[position].imageLarge).apply(RequestOptions().override(128)).into(holder.view.findViewById(R.id.image))

        holder.view.setOnClickListener {setGroupSelected(position)}
        product.setOnClickListener {setGroupSelected(position)}
    }

    private fun setGroupSelected(selectedPos: Int) {
        model.setCurrent(selectedPos)
        Toast.makeText(fragment.context, "Set as selected item", Toast.LENGTH_SHORT).show()
        Log.d("alt", "Setting selected position as $selectedPos for group ${model.currentGroupId}")
    }

    override fun getItemCount(): Int {
        return items.size
    }


}
package com.stockapp

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.JsonSyntaxException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.NumberFormat
import java.util.*

class StoreAdapter(var fragment: Fragment, val model: ProductViewModel): RecyclerView.Adapter<StoreAdapter.StoreViewHolder>() {
    inner class StoreViewHolder(val view: View): RecyclerView.ViewHolder(view)

    init {
        val sharedPref = fragment.requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "")
        RetrofitService.create(MainActivity.url).getProducts(token!!)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                Log.d("cart/prod", "Received result $res")
                EStore.values().forEach { store ->
                    stores[store.index].updateItems((res.filter {it.store == store.name}).groupBy{it.productGroupId})
                    stores[store.index].updatePrice()
                }
//                stores[0].updateItems((res.filter {it.store == "Kroger"}).groupBy{it.productGroupId})
//                stores[1].updateItems((res.filter {it.store == "Target"}).groupBy{it.productGroupId})
            },
                { error ->
                    if (error is JsonSyntaxException)
                    {
                        Toast.makeText(fragment.context, "Error establishing account credentials.", Toast.LENGTH_LONG).show()
                        Log.e("StoreAdapter", "JsonSyntaxException thrown: $error")
                        with(sharedPref.edit()) {
                            remove("token")
                            apply()
                        }
                        fragment.findNavController().popBackStack(R.id.storesFragment, true)
                        fragment.findNavController().navigate(R.id.loginPopupFragment)
                    }
                    else {
                        Log.e("Stores", "Error fetching store specific products: $error")
                        Toast.makeText(fragment.context,"There was an issue getting the items.",Toast.LENGTH_SHORT).show()
                    }
                })
    }

    inner class Store(val name: String, val pic: String, var price: String, var items: Map<Int, List<CartItem>>, var adapter: InnerAdapter?, var selectedItems: MutableList<Int>, var viewHolder: StoreViewHolder?)
    {
        fun updateItems(map: Map<Int, List<CartItem>>) {
            adapter!!.notifyItemRangeChanged(0, items.size)
            items = map
            adapter!!.items = map
            selectedItems = MutableList(map.keys.size) {0}
            adapter!!.notifyItemRangeChanged(0, map.size)
            Log.d("adapter", "Modified inner adapter for store $name, new list is $map")
            Log.d("adapter", "After above modification, map key list is size ${map.keys.size} and selectedItems is ${selectedItems}")
            updatePrice()
        }

        fun updatePrice() {
            val total = adapter?.getTotalPrice() ?: return
            Log.d("StoreAdapt", "Updating price for $name, new price is $total")
            price = model.formatPrice(total)
            viewHolder!!.view.findViewById<TextView>(R.id.price).text = price
        }
    }

    var stores: List<Store> = listOf(
        Store("Kroger",
            "https://prnewswire2-a.akamaihd.net/p/1893751/sp/189375100/thumbnail/entry_id/0_kwshpnaa/def_height/2700/def_width/2700/version/100012/type/1",
            "",
            mapOf(),
            null,
            mutableListOf(),
            null
        ),
        Store("Target",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Target_logo.svg/1541px-Target_logo.svg.png",
            "",
            mapOf(),
            null,
            mutableListOf(),
            null
        ),
        Store("Walmart",
            "https://s3.amazonaws.com/www-inside-design/uploads/2018/04/walmart-square.jpg",
            "",
            mapOf(),
            null,
            mutableListOf(),
            null
        ),
        Store("Amazon",
            "https://www.doorwaysva.org/wp-content/uploads/2019/06/amazon-logo.png",
            "",
            mapOf(),
            null,
            mutableListOf(),
            null
        )
    )
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.store_card, parent, false)
        return StoreViewHolder(v)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {

        Glide.with(fragment).load(stores[position].pic).apply(RequestOptions().override(256, 256)).into(holder.view.findViewById(R.id.image))

        val adapter = InnerAdapter(stores[position])
        adapter.items = stores[position].items
        Log.d("outer", "Binding initial viewholder. selectedItems is ${stores[position].selectedItems}")
        stores[position].adapter = adapter
        stores[position].viewHolder = holder
        val recView = holder.view.findViewById<RecyclerView>(R.id.recview)
        recView.layoutManager = LinearLayoutManager(fragment.context, LinearLayoutManager.HORIZONTAL, false)
        recView.adapter = adapter

        stores[position].updatePrice()
        holder.view.findViewById<TextView>(R.id.storeName).text = stores[position].name
    }

    inner class InnerAdapter(val store: Store): RecyclerView.Adapter<InnerAdapter.InnerViewHolder>() {
        inner class InnerViewHolder(val view: View): RecyclerView.ViewHolder(view)

        var items: Map<Int, List<CartItem>> = mapOf()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.store_item_card, parent, false)
            return InnerViewHolder(v)
        }

        override fun onBindViewHolder(holder: InnerViewHolder, position: Int) {
            Log.d("inner", "Binding viewholder for store ${store.name}, position $position")
            val key = items.keys.toList()[position]
            val selectedItem = store.selectedItems[position]
            val item = items[key]!![selectedItem]
            Glide.with(fragment).load(item.imageLarge).apply(RequestOptions().override(128, 128)).into(holder.view.findViewById(R.id.image))

            holder.view.findViewById<ImageView>(R.id.image).setOnClickListener {
                Log.d("inner", "Item being shown: $item")
                model.currentStore = EStore.valueOf(store.name)
                model.currentGroupId = key
                fragment.findNavController().navigate(R.id.alternativesFragment)
            }
        }

        override fun getItemCount(): Int {
            return items.keys.size
        }

        fun getTotalPrice(): Float {
            var sum = 0F
            (items.keys.toList() zip store.selectedItems).forEach { pair ->
                sum += items[pair.first]!![pair.second].price
            }
            return sum
        }
    }

    override fun getItemCount(): Int {
        return stores.size
    }


}
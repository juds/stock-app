package com.stockapp

data class NewUser(
    val name: String,
    val password: String,
    val email: String,
    val zipcode: String
)
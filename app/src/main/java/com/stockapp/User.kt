package com.stockapp

import java.util.*

class User(var name: String, var password: String) {

    fun encode(): String {
        return "Basic ${Base64.getEncoder().encodeToString("$name:$password".toByteArray())}"
    }

}
package com.stockapp

import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface RetrofitService {

    @GET("product/{search}")
    fun searchProducts(@Path("search") search: String, @Header("x-access-token") authToken: String): Observable<List<Item>>

    @POST("cart")
    fun addToCart(@Body item: Item, @Header("x-access-token") authToken: String): Call<Void>

    @HTTP(method = "DELETE", path = "cart", hasBody = true)
    fun removeFromCart(@Body description: String, @Header("x-access-token") authToken: String): Call<Void>

    @POST("favorite")
    fun addToFavs(@Body item: Item, @Header("x-access-token") authToken: String): Call<Void>

    @HTTP(method = "DELETE", path = "favorite", hasBody = true)
    fun removeFromFavs(@Body description: String, @Header("x-access-token") authToken: String): Call<Void>

    @GET("login")
    fun getLoginToken(@Header("Authorization") encoded: String): Observable<Token>

    @POST("user")
    fun createNewUser(@Body user: NewUser): Call<User>

    @GET("cart/products")
    fun getProducts(@Header("x-access-token") authToken: String): Observable<List<CartItem>>

    @POST("group")
    fun getGroup(@Body group: String): Observable<List<CartItem>>

    @GET("userinfo")
    fun getUser(@Header("x-access-token") token: String): Call<NetworkUser>

    companion object {
        fun create(baseUrl: String): RetrofitService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .client(OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build())
                .baseUrl(baseUrl).build()

            return retrofit.create(RetrofitService::class.java)
        }
    }
}
package com.stockapp

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(item: Item)

    @Query("DELETE FROM item_table WHERE description = :description")
    fun delete(description: String)

    @Query("SELECT * FROM item_table")
    fun getAll(): LiveData<List<Item>>

    @Query("SELECT EXISTS (SELECT 1 FROM item_table WHERE description = :description)")
    fun contains(description: String): LiveData<Boolean>
}
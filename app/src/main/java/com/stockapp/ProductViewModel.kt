package com.stockapp

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class ProductViewModel(application: Application): AndroidViewModel(application) {

    //Coroutine fields
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob+ Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    fun register(user: NewUser, activity: MainActivity, navController: NavController) = scope.launch(Dispatchers.IO) {
        val res = RetrofitService.create(MainActivity.url).createNewUser(user).execute()
        when (res.code())
        {
            200 -> activity.runOnUiThread {
                Toast.makeText(activity, "Successfully registered account!", Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
            else -> {
                activity.runOnUiThread {
                    Toast.makeText(activity, "There was an error.", Toast.LENGTH_SHORT).show()
                    Log.e("register", "Error code ${res.code()}, message: ${res.message()}")
                }
            }
        }
    }

    var user: MutableLiveData<NetworkUser> = MutableLiveData<NetworkUser>()

    fun getUser(token: String, activity: MainActivity, navController: NavController, sharedPref: SharedPreferences) = scope.launch(Dispatchers.IO) {
        try {
            val res = RetrofitService.create(MainActivity.url).getUser(token).execute()
            when (res.code())
            {
                200 -> activity.runOnUiThread {
                    user.value = res.body()
                }
                else -> activity.runOnUiThread {
                    Toast.makeText(activity, "Error establishing account credentials.", Toast.LENGTH_LONG).show()
                    Log.e("PVM getUser", "HTTP Error code ${res.code()}. Message was ${res.message()}")
                    with(sharedPref.edit()) {
                        remove("token")
                        apply()
                    }
                    navController.navigate(R.id.logout)
                }
            }
        }
        catch (error: JsonSyntaxException) {
            activity.runOnUiThread {
                Toast.makeText(activity, "Error establishing account credentials.", Toast.LENGTH_LONG).show()
                Log.e("PVM getUser", "JSON error: $error.")
                with(sharedPref.edit()) {
                    remove("token")
                    apply()
                }
                navController.navigate(R.id.logout)
            }
        }
    }

    private val queryRepo: QueryRepository = QueryRepository(QueryRoomDatabase.getDatabase(application).queryDao(), application.applicationContext)

    fun queryInsert(query: SearchQuery) = scope.launch(Dispatchers.IO) { queryRepo.insert(query) }
    fun queryDelete(query: SearchQuery) = scope.launch(Dispatchers.IO) { queryRepo.delete(query) }
    fun queryDeleteAll() = scope.launch(Dispatchers.IO) { queryRepo.deleteAll() }
    var queries = queryRepo.queries

    fun pretty(str: String) = str.split(' ').joinToString(" ") {it.replaceFirstChar(Char::titlecase)}

    var selectedGroup: String = ""

    //Cart item repository
    private val cartRepo: CartItemRepository = CartItemRepository(CartRoomDatabase.getDatabase(application).cartDao(), application.applicationContext)

    //Items currently in the cart
    var cartItems: LiveData<List<Item>> = cartRepo.cart

    fun cartInsert(item: Item) = scope.launch(Dispatchers.IO) {
        adapter = null
        cartRepo.insert(item)
    }

    fun cartDelete(item: Item) = scope.launch(Dispatchers.IO) {
        adapter = null
        cartRepo.deleteItem(item)
    }

    private val favRepo: FavoritesRepository = FavoritesRepository(
        FavoritesRoomDatabase.getDatabase(application).favDao(),
        application.applicationContext
    )

    //items currently favorited
    var favItems: LiveData<List<Item>> = favRepo.favorites

    fun favInsert(item: Item) = scope.launch(Dispatchers.IO) {
        favRepo.insert(item)
    }

    fun favDelete(item: Item) = scope.launch(Dispatchers.IO) {
        favRepo.deleteItem(item)
    }

    fun isFavorite(item: Item) = favRepo.contains(item)

    var adapter: StoreAdapter? = null

    lateinit var currentStore: EStore
    var currentGroupId: Int = -1

    fun setCurrent(selectedPos: Int) {
        val store = adapter!!.stores[currentStore.index]
        val index = store.adapter!!.items.keys.toList().indexOf(currentGroupId)
        store.selectedItems[index] = selectedPos
    }

    fun getCurrentStore(): StoreAdapter.Store {
        return adapter!!.stores[currentStore.index]
    }

    val format = NumberFormat.getCurrencyInstance().let {
        it.maximumFractionDigits = 2
        it.currency = Currency.getInstance("USD")
        it
    }

    fun formatPrice(price: Float): String = format.format(price)

}
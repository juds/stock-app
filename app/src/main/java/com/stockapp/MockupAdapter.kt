package com.stockapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class MockupAdapter(val fragment: Fragment): RecyclerView.Adapter<MockupAdapter.StoreViewHolder>() {
    inner class StoreViewHolder(val view: View): RecyclerView.ViewHolder(view)

    inner class Store(val name: String, val pic: String, val dist: String, val price: String, val items: List<String>)

    var stores: List<Store> = listOf(
        Store("Kroger",
            "https://prnewswire2-a.akamaihd.net/p/1893751/sp/189375100/thumbnail/entry_id/0_kwshpnaa/def_height/2700/def_width/2700/version/100012/type/1",
            "2 miles",
            "$8.58",
            listOf(
                "https://www.kroger.com/product/images/large/back/0001111041700",
                "https://www.kroger.com/product/images/large/front/0001111050712",
                "https://www.kroger.com/product/images/large/front/0000000004011"
        )),
        Store("Aldi",
            "https://corporate.aldi.us/fileadmin/fm-dam/logos/ALDI_2017.png",
            "4.7 miles",
            "$10.25",
            listOf(
                "https://www.aldi.us/fileadmin/_processed_/c/3/csm_5450-FriendlyFarms-WholeMilk-Detail-Exported_44c61fe38f.jpg",
                "https://www.aldi.us/fileadmin/_processed_/c/6/csm_061417_R_45690_SPS_PremiumIceCream_Chocolate_Side_D_aa4be9a9ea.jpg",
                "https://www.aldi.us/fileadmin/_processed_/a/c/csm_Banana_D_d6bd30e53c.jpg"
            )
        ),
        Store("Walmart",
            "https://cdn.mos.cms.futurecdn.net/5StAbRHLA4ZdyzQZVivm2c.jpg",
            "3 miles",
            "$8.24",
            listOf(
                "https://i5.walmartimages.com/asr/83f533c3-3234-4bea-80bf-a0f9a43cd279_2.9b223f40bab27c513ba64f9f0e3fc2d9.jpeg",
                "https://i5.walmartimages.com/asr/3e6d6d0b-1f16-428d-a22e-0754ec3f9a9f_1.7f2023d79c221d4cf60c55faf1e0e1a2.jpeg",
                "https://i5.walmartimages.com/asr/93a17e9f-c519-487e-8f61-6f7b58fc764f.07776d55da72f8818cec403ec35ff81c.jpeg?odnHeight=612&odnWidth=612&odnBg=FFFFFF"
            )
        )
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.comparison_mockup_card, parent, false)
        return StoreViewHolder(v)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        holder.view.findViewById<ImageButton>(R.id.image)
        holder.view.findViewById<TextView>(R.id.storeName).text = stores[position].name
        holder.view.findViewById<TextView>(R.id.storeDistance).text = stores[position].dist
        holder.view.findViewById<TextView>(R.id.price).text = stores[position].price

        Glide.with(fragment).load(stores[position].pic).apply(RequestOptions().override(256, 256)).into(holder.view.findViewById(R.id.image))

        val adapter = MockupInnerAdapter()
        adapter.pics = stores[position].items
        val recView = holder.view.findViewById<RecyclerView>(R.id.recview)
        recView.layoutManager = LinearLayoutManager(fragment.context, LinearLayoutManager.HORIZONTAL, false)
        recView.adapter = adapter
    }

    inner class MockupInnerAdapter: RecyclerView.Adapter<MockupInnerAdapter.InnerViewHolder>() {
        inner class InnerViewHolder(val view: View): RecyclerView.ViewHolder(view)

        var pics: List<String> = listOf()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.comparison_item_mockup_card, parent, false)
            return InnerViewHolder(v)
        }

        override fun onBindViewHolder(holder: InnerViewHolder, position: Int) {
            Glide.with(fragment).load(pics[position]).apply(RequestOptions().override(128, 128)).into(holder.view.findViewById(R.id.image))
        }

        override fun getItemCount(): Int {
            return pics.size
        }
    }

    override fun getItemCount(): Int {
        return stores.size
    }


}
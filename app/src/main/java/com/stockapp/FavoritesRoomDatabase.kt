package com.stockapp

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Item::class], version = 6, exportSchema = false)
abstract class FavoritesRoomDatabase: RoomDatabase() {

    abstract fun favDao(): ItemDao

    companion object {
        @Volatile
        private var INSTANCE: FavoritesRoomDatabase? = null

        fun getDatabase(context: Context): FavoritesRoomDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) return tempInstance

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FavoritesRoomDatabase::class.java,
                    "Favorites_database")
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }

}
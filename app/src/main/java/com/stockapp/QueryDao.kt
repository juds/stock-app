package com.stockapp

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface QueryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertQuery(query: SearchQuery)

    @Query("SELECT * FROM queries")
    fun getAll(): LiveData<MutableList<SearchQuery>>

    @Query("DELETE FROM queries WHERE text = :text")
    fun delete(text: String)

    @Query("DELETE FROM queries")
    fun deleteAll()
}

package com.stockapp

import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.NavController
import java.lang.IllegalStateException

class LogoutDialogFragment(val navController: NavController, val sharedPref: SharedPreferences): DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Are you sure you'd like to log out?")
                .setPositiveButton("Yes") { dialog, _ ->
                    with(sharedPref.edit()) {
                        remove("token")
                        apply()
                    }
                    dialog.dismiss()
                    navController.navigate(R.id.logout)
                }
                .setNegativeButton("No") { dialog, _ ->
                    dialog.cancel()
                }

            return builder.create()
        } ?: throw IllegalStateException("Activity cannot be null.")
    }

}
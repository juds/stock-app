package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentAlternativesBinding

class AlternativesFragment : Fragment() {

    private var _binding: FragmentAlternativesBinding? = null
    private val binding get() = _binding!!

    val model: ProductViewModel  by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentAlternativesBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Product Options")

        val adapter = AltAdapter(model, this)
        adapter.items = model.getCurrentStore().items[model.currentGroupId]!!
        val recView = binding.recyclerView
        recView.adapter = adapter
        recView.layoutManager = LinearLayoutManager(context)

        // Inflate the layout for this fragment
        return binding.root
    }
}
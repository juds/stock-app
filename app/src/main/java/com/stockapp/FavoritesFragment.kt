package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentFavoritesBinding
import kotlinx.android.synthetic.main.activity_main.*

class FavoritesFragment : Fragment() {

    private var _binding: FragmentFavoritesBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Favorites")
        (activity as MainActivity).bottomNavigationView.menu.getItem(1).isChecked = true

        val adapter = FavoritesAdapter(model, this)
        val recView = binding.recView
        recView.layoutManager = LinearLayoutManager(context)
        recView.adapter = adapter

        model.favItems.observe(viewLifecycleOwner) { items ->
            items?.let {adapter.setItems(it)}
            if (items.isEmpty())
            {
                binding.textView.visibility = View.VISIBLE
                binding.textView2.visibility = View.VISIBLE
                binding.textView3.visibility = View.VISIBLE
            }
        }

        return binding.root
    }
}
package com.stockapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class FavoritesAdapter(val model: ProductViewModel, val fragment: Fragment):  RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder>() {
    inner class FavoritesViewHolder(val view: View): RecyclerView.ViewHolder(view)

    private var items = mutableListOf<Item>()

    internal fun setItems(items: List<Item>)
    {
        this.items = items.toMutableList()
        notifyItemRangeChanged(0, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.favorites_card, parent, false)
        return FavoritesViewHolder(v)
    }

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        val product = holder.view.findViewById<TextView>(R.id.product)
        product.text = items[position].description
        product.isSelected = true
        holder.view.findViewById<TextView>(R.id.price).text = model.formatPrice(items[position].price)

        Glide.with(fragment).load(items[position].image).apply(RequestOptions().override(128, 128)).into(holder.view.findViewById(R.id.image))

        holder.view.findViewById<ImageButton>(R.id.addCartButton).setOnClickListener {
            model.cartInsert(items[position])
        }

        val favButt = holder.view.findViewById<ImageButton>(R.id.favButton)
        favButt.setOnClickListener {
            favButt.setImageResource(R.drawable.ic_fav_border)
            model.favDelete(items[position])
            items.removeAt(position)
            notifyItemRemoved(position)
        }

        holder.view.findViewById<ConstraintLayout>(R.id.layout).setOnClickListener {
            navigate(items[position].description)
        }
    }

    private fun navigate(description: String) {
        model.selectedGroup = description
        Log.d("FavAdapter", "Navigating to group preview with description $description")
        fragment.findNavController().navigate(R.id.groupPreviewFragment)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}
package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentGroupPreviewBinding

class GroupPreviewFragment : Fragment() {

    private var _binding: FragmentGroupPreviewBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentGroupPreviewBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle(model.pretty(model.selectedGroup))

        val adapter = GroupPreviewAdapter(this, model)
        val recView = binding.recyclerView
        recView.adapter = adapter
        recView.layoutManager = LinearLayoutManager(context)
        adapter.executeSearch(model.selectedGroup)

        // Inflate the layout for this fragment
        return binding.root
    }
}
package com.stockapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.stockapp.databinding.FragmentLoginPopupBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginPopupFragment : Fragment() {

    private var _binding: FragmentLoginPopupBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentLoginPopupBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Login")

        binding.login.setOnClickListener {
            val user = User(binding.textUsername.text.toString(), binding.textPassword.text.toString())
            RetrofitService.create(MainActivity.url).getLoginToken(user.encode())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({ token ->
                    Toast.makeText(context, "Successfully logged in.", Toast.LENGTH_SHORT).show()
                    val sharedPref = requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
                    with (sharedPref.edit()) {
                        putString("token", token.token)
                        apply()
                    }
                    findNavController().navigate(R.id.logged_in)
                },
                    {
                        Log.e("Login", "Error fetching token: $it. Auth code was: ${user.encode()}")
                        Toast.makeText(context, "There was an error logging in.", Toast.LENGTH_SHORT).show()
                    })
        }

        binding.register.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }

        // Inflate the layout for this fragment
        return binding.root
    }
}
package com.stockapp

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [SearchQuery::class], version = 1, exportSchema = false)
abstract class QueryRoomDatabase: RoomDatabase() {

    abstract fun queryDao(): QueryDao

    companion object {
        @Volatile
        private var INSTANCE: QueryRoomDatabase? = null

        fun getDatabase(context: Context): QueryRoomDatabase {
            val tempInstance = INSTANCE

            if (tempInstance != null) return tempInstance

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    QueryRoomDatabase::class.java,
                    "query_database"
                ).fallbackToDestructiveMigration()
                .build()
                INSTANCE = instance
                instance
            }
        }

    }

}
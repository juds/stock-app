package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentCartBinding
import kotlinx.android.synthetic.main.activity_main.*

class CartFragment : Fragment() {

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentCartBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Cart")
        (activity as MainActivity).bottomNavigationView.menu.getItem(3).isChecked = true

        val adapter = CartAdapter(model, this)
        val recView = binding.recView
        recView.layoutManager = LinearLayoutManager(context)
        recView.adapter = adapter

        binding.button2.setOnClickListener {
            findNavController().navigate(R.id.storesFragment)
        }

        model.cartItems.observe(viewLifecycleOwner) { items ->
            items?.let { adapter.setCart(items) }
            adapter.notifyItemRangeChanged(0, adapter.itemCount)
            if (items.isEmpty())
            {
                binding.textView.visibility = View.VISIBLE
                binding.textView2.visibility = View.VISIBLE
                binding.textView3.visibility = View.VISIBLE
            }
        }

        return binding.root
    }
}
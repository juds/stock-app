package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.stockapp.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Register")

        binding.create.setOnClickListener {
            val user = NewUser(
                binding.textUsername.text.toString(),
                binding.textPassword.text.toString(),
                binding.textEmail.text.toString(),
                binding.textZipCode.text.toString()
            )
            model.register(user, activity as MainActivity, findNavController())
        }

        // Inflate the layout for this fragment
        return binding.root
    }


}
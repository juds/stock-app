package com.stockapp

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GroupPreviewAdapter(val fragment: Fragment, val model: ProductViewModel): RecyclerView.Adapter<GroupPreviewAdapter.ItemViewHolder>() {

    private var items: List<CartItem> = listOf()

    fun executeSearch(description: String)
    {
        val sharedPref = fragment.requireContext().getSharedPreferences("token", Context.MODE_PRIVATE)
        val token = sharedPref.getString("token", "") ?: ""
        RetrofitService.create(MainActivity.url).getGroup(description).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                result -> setItems(result!!)
            },
            {
                error -> showError(error)
            })
    }

    private fun showError(error: Throwable?) {
        Log.e("GPAdapter", "Error fetching from API: $error")
        Toast.makeText(fragment.context, "Error getting items.", Toast.LENGTH_LONG).show()
    }

    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): GroupPreviewAdapter.ItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.group_preview_card, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: GroupPreviewAdapter.ItemViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.product).text = items[position].description
        holder.view.findViewById<TextView>(R.id.product).isSelected = true
        Glide.with(fragment).load(items[position].imageLarge).apply(RequestOptions().override(128)).into(holder.view.findViewById(R.id.image))
        holder.view.findViewById<TextView>(R.id.storeName).text = items[position].store
        holder.view.findViewById<TextView>(R.id.price).text = model.formatPrice(items[position].price)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(result: List<CartItem>)
    {
        notifyItemRangeChanged(0, itemCount)
        items = result
        notifyItemRangeChanged(0, itemCount)
    }

    inner class ItemViewHolder(val view: View): RecyclerView.ViewHolder(view)
}
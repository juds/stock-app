package com.stockapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockapp.databinding.FragmentPriceComparisonMockupBinding

class PriceComparisonMockupFragment : Fragment() {

    private var _binding: FragmentPriceComparisonMockupBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        _binding = FragmentPriceComparisonMockupBinding.inflate(inflater, container, false)

        (activity as MainActivity).changeTitle("Checkout Options")

        val adapter = MockupAdapter(this)
        val recView = binding.recview
        recView.layoutManager = LinearLayoutManager(context)
        recView.adapter = adapter

        // Inflate the layout for this fragment
        return binding.root
    }
}
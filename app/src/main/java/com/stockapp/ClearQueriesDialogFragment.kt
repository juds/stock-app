package com.stockapp

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import java.lang.IllegalStateException

class ClearQueriesDialogFragment: DialogFragment() {

    private val model: ProductViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage("You are about to clear your search history. This action is not reversable.")
                .setPositiveButton("Continue") { dialog, _ ->
                    model.queryDeleteAll()
                    dialog.dismiss()
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}